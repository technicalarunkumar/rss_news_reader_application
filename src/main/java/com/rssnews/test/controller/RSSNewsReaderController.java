package com.rssnews.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rssnews.test.service.RssNewsReaderService;

@RestController
@RequestMapping("/rssnewsreader")
public class RSSNewsReaderController {
	
	
	@Autowired
	private  RssNewsReaderService  service;
	

	@GetMapping("/topstories")
	public ResponseEntity<String>  getTopStories(){
		ResponseEntity< String>  entity=new ResponseEntity<String>(service.readNewsFromThirdParty(),HttpStatus.OK);  //(body, status)
		return entity;
	}
	
}
