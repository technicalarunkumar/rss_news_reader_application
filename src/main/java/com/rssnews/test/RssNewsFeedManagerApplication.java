package com.rssnews.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RssNewsFeedManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RssNewsFeedManagerApplication.class, args);
	}

}
