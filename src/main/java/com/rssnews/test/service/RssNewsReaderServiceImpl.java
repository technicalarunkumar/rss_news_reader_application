package com.rssnews.test.service;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;


import org.springframework.stereotype.Service;

import com.rssnews.test.model.Rss;
import com.rssnews.test.utils.ReaderUtils;

@Service
public class RssNewsReaderServiceImpl implements RssNewsReaderService {

	
	
	@Override
	public String readNewsFromThirdParty() {
		
		try {
			String receivedXMLString=ReaderUtils.getNewsContent();
			
			StringReader sr = new StringReader(receivedXMLString);
			JAXBContext jaxbContext = JAXBContext.newInstance(Rss.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Rss response = (Rss) unmarshaller.unmarshal(sr);
			
			System.out.println(response);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ReaderUtils.getNewsContent();
	}

}
