package com.rssnews.test.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Value;

public class ReaderUtils {

	
	@Value("${news.url}")
	private static String newsURL;
	
	@Value("${news.content.type}")
	private static String contentType;
	
	public static String getNewsContent() {
		StringBuffer xmlResponseData = new StringBuffer();
		try {
			// Url for making POST request
			URL postUrl = new URL("https://cio.economictimes.indiatimes.com/rss/topstories");
			HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection();        
			// Set POST as request method
			connection.setRequestMethod("POST");        
			// Setting Header Parameters
			connection.setRequestProperty("Content-Type","application/xml");
			connection.setUseCaches(false);
			connection.setDoOutput(true);
			BufferedWriter wr = new BufferedWriter(
			    new OutputStreamWriter( connection.getOutputStream(), "UTF-8"));
			//wr.write(params);
			wr.close();
			connection.connect();
			
			
		    String readLine = null;
		    BufferedReader bufferedReader = new BufferedReader(
		        new InputStreamReader(connection.getInputStream()));
			
		    while ((readLine = bufferedReader.readLine()) != null) {
		    	xmlResponseData.append(readLine + "\n");
		    }
			
		    bufferedReader.close();
		    //System.out.println(xmlResponseData.toString());
		}  catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return xmlResponseData.toString();
	}
	
}
