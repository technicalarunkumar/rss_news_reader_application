package com.rssnews.test.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "channel")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Channel {

	private static final long serialVersionUID = 1L;
	
	private String title;
	private String link;
	private String description;
	private String language;
	
	private List<Item> item;
	
	
	public Channel() {
		super();
	}
	
	public Channel(String title,String link,String description,String language,List<Item> item) {
		this.title=title;
		this.link=link;
		this.description=description;
		this.language=language;
		this.item=item;			
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public List<Item> getItem() {
		return item;
	}

	public void setItem(List<Item> item) {
		this.item = item;
	}
	
	@Override
	  public String toString() {
	    return "Item [title=" + title+ ",link" +link+",description" +description+ ",language" +language+",item" +item.size()+ "]";
	  }
}
