package com.rssnews.test.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "item")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Item {

	private static final long serialVersionUID = 1L;
	
	
	private String title;
	private String link;
	private String guid;
	private String description;
	//private Image image;
	private String pubDate;
	
	public Item() {
		super();
	}
	
	public Item(String title,String link,String guid,String description,String pubDate) {
		this.title=title;
		this.link=link;
		this.guid=guid;
		this.description=description;
		this.pubDate=pubDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	
	@Override
	  public String toString() {
	    return "Item [title=" + title+ ",link" +link+ ",guid" +guid+",description" +description+ ",pubDate" +pubDate+ "]";
	  }
}
