package com.rssnews.test.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "rss")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Rss implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Channel channel;
	
	public Rss() {
		super();
	}
	
	public Rss(Channel channel) {
	    super();
	    this.channel = channel;
	  }

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}
	
	@Override
	  public String toString() {
	    return "Rss [channel=" + channel+ "]";
	  }
}
